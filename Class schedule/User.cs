﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Drawing;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Threading;

namespace Class_Schedule
{
    public partial class User : Form
    {
        double ktoryWeek;
        List<Class> zajecia = new List<Class>();
        List<Student> studenci = new List<Student>();
        Thread t;
        private delegate void StepPassedEventHandler();
        public User()
        {
            InitializeComponent();
            updateFont();
            checkIfFilesPresent();
            checkConnection();
            label1.Text = DateTime.Now.ToString("MMM ddd d \n HH:mm:ss");
            t = new Thread(clock);
            t.Start();
        }
        private void clock()
        {
            StepPassedEventHandler delegat = updateTime;
            while (true)
            {
                Thread.Sleep(1000);
                Invoke(delegat);
            }
        }
        private void updateTime()
        {
            label1.Text = DateTime.Now.ToString("MMM ddd d \n HH:mm:ss");
        }
        private List<Student> loadStudenci()
        {
            string file = "studenci.xml";
            List<Student> listofa = new List<Student>();
            XmlSerializer formatter = new XmlSerializer(typeof(List<Student>));
            FileStream aFile = new FileStream(file, FileMode.Open);
            byte[] buffer = new byte[aFile.Length];
            aFile.Read(buffer, 0, (int)aFile.Length);
            MemoryStream stream = new MemoryStream(buffer);
            return (List<Student>)formatter.Deserialize(stream);
        }
        private void saveStudenci(List<Student> listofa)
        {
            string path = "studenci.xml";
            FileStream outFile = File.Create(path);
            XmlSerializer formatter = new XmlSerializer(typeof(List<Student>));
            formatter.Serialize(outFile, listofa);
        }
        private List<Class> loadZajecia()
        {
            string file = "zajecia.xml";
            List<Class> listofa = new List<Class>();
            XmlSerializer formatter = new XmlSerializer(typeof(List<Class>));
            FileStream aFile = new FileStream(file, FileMode.Open);
            byte[] buffer = new byte[aFile.Length];
            aFile.Read(buffer, 0, (int)aFile.Length);
            MemoryStream stream = new MemoryStream(buffer);
            return (List<Class>)formatter.Deserialize(stream);
        }
        private void saveZajecia(List<Class> listofa)
        {
            string path = "zajecia.xml";
            FileStream outFile = File.Create(path);
            XmlSerializer formatter = new XmlSerializer(typeof(List<Class>));
            formatter.Serialize(outFile, listofa);
        }
        private void checkIfFilesPresent()
        {
            string zajeciaFile = "zajecia.xml";
            string studenciFile = "studenci.xml";
            if ((File.Exists(zajeciaFile) && File.Exists(studenciFile)) != true)
                loadButton.Enabled = false;
        }
        private void checkConnection()
        {
            try
            {
                string connString = "Server=localhost;Uid=root;Pwd=1234;Database=pwsz;";
                MySqlConnection connection = new MySqlConnection(connString);
                connection.Open();
                connection.Close();
            }
            catch
            {
                MessageBox.Show("Brak połączenia z bazą danych");
                connectButton.Enabled = false;
                adminButton.Enabled = false;
            }
        }
        private void loadStudents()
        {
            string temp = "";
            foreach (Student s in studenci)
            {
                temp += s.id + ". " + s.name + " " + s.secondname + " " + s.surname;
                if (s.grupa == "A1")
                    listBoxA1.Items.Add(s);
                else if (s.grupa == "A2")
                    listBoxA2.Items.Add(s);
                else if (s.grupa == "B1")
                    listBoxB1.Items.Add(s);
                temp = "";
            }

            listBoxA1.DisplayMember = "DisplayForm";
            listBoxA2.DisplayMember = "DisplayForm";
            listBoxB1.DisplayMember = "DisplayForm";
        }
        public void loadDB()
        {
            string connString = "Server=localhost;Uid=root;Pwd=1234;Database=pwsz;";
            MySqlConnection connection = new MySqlConnection(connString);
            connection.Open();
            string queryS = "SELECT * FROM students";
            var cmdS = new MySqlCommand(queryS, connection);
            var reader = cmdS.ExecuteReader();
            while (reader.Read())
            {
                Student student = new Student(reader.GetInt16(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
                studenci.Add(student);
            }
            connection.Close();
            connection.Open();
            string queryC = "SELECT *  FROM classes";
            var cmdC = new MySqlCommand(queryC, connection);
            reader = cmdC.ExecuteReader();
            while (reader.Read())
            {
                Class zajecie = new Class(reader.GetInt16(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetDateTime(4), reader.GetString(5), reader.GetString(6), reader.GetInt16(7));
                zajecia.Add(zajecie);
            }
            connection.Close();
            saveStudenci(studenci);
            saveZajecia(zajecia);
            connectButton.Enabled = false;
            loadButton.Enabled = false;
        }
        private void showButton_Click(object sender, EventArgs e)
        {
            ktoryWeek = 0;
            drukujPlan();
        }
        private void nextWeekButton_Click(object sender, EventArgs e)
        {
            ktoryWeek++;
            drukujPlan();
        }
        private void previousWeekButton_Click(object sender, EventArgs e)
        {
            ktoryWeek--;
            drukujPlan();
        }
        private void updateFont()
        {
            foreach (DataGridViewColumn c in dataGridView1.Columns)
                c.DefaultCellStyle.Font = new Font("Arial", 11F, GraphicsUnit.Pixel);
        } // just datagrid font
        private void drukujPlan()
        {
            string grupa;
            if (radioA1.Checked == true) grupa = "A1";
            else if (radioA2.Checked == true) grupa = "A2";
            else grupa = "B1";

            DateTime lowerTime = DateTime.Now;
            lowerTime = lowerTime.AddDays(7 * ktoryWeek);
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(lowerTime);
            double realDay = (int)day;
            if (realDay == 0)
                realDay = 7;
            lowerTime = lowerTime.AddDays(-realDay + 1);
            DateTime upperTime = lowerTime.AddDays(6);
            lowerTime = lowerTime.Date;
            upperTime = upperTime.Date;
            label2.Text = lowerTime.Month + "." + lowerTime.Day;
            label3.Text = upperTime.Month + "." + upperTime.Day;
            upperTime = upperTime.AddDays(1);
            string[,] tab = new string[7, 7];

            foreach (Class z in zajecia)
            {
                if ((grupa == z.grupa || z.type == "w.") && (z.dateTime <= upperTime && z.dateTime >= lowerTime))
                {
                    DayOfWeek dayOfClass = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(z.dateTime);
                    switch ((int)dayOfClass)
                    {
                        case 1:
                            checkFor(tab, 0, z);
                            break;
                        case 2:
                            checkFor(tab, 1, z);
                            break;
                        case 3:
                            checkFor(tab, 2, z);
                            break;
                        case 4:
                            checkFor(tab, 3, z);
                            break;
                        case 5:
                            checkFor(tab, 4, z);
                            break;
                        case 6:
                            checkFor(tab, 5, z);
                            break;
                        case 7:
                            checkFor(tab, 6, z);
                            break;
                    }
                }
            }
            dataGridView1.Rows.Clear();
            dataGridView1.Rows.Add("8:00-9:30", tab[0, 0], tab[1, 0], tab[2, 0], tab[3, 0], tab[4, 0], tab[5, 0], tab[6, 0]);
            dataGridView1.Rows.Add("9:40-11:10", tab[0, 1], tab[1, 1], tab[2, 1], tab[3, 1], tab[4, 1], tab[5, 1], tab[6, 1]);
            dataGridView1.Rows.Add("11:20-12:50", tab[0, 2], tab[1, 2], tab[2, 2], tab[3, 2], tab[4, 2], tab[5, 2], tab[6, 2]);
            dataGridView1.Rows.Add("13:20-14:50", tab[0, 3], tab[1, 3], tab[2, 3], tab[3, 3], tab[4, 3], tab[5, 3], tab[6, 3]);
            dataGridView1.Rows.Add("15:00-16:30", tab[0, 4], tab[1, 4], tab[2, 4], tab[3, 4], tab[4, 4], tab[5, 4], tab[6, 4]);
            dataGridView1.Rows.Add("16:40-18:10", tab[0, 5], tab[1, 5], tab[2, 5], tab[3, 5], tab[4, 5], tab[5, 5], tab[6, 5]);
            dataGridView1.Rows.Add("18:20-19:50", tab[0, 6], tab[1, 6], tab[2, 6], tab[3, 6], tab[4, 6], tab[5, 6], tab[6, 6]);

        }
        private string wyssijStringa(Class z)
        {
            DateTime endTime = z.dateTime.AddMinutes(45 * z.length);
            return z.dateTime.Hour.ToString() + ":" + z.dateTime.Minute.ToString("00.##") + "-" + endTime.Hour.ToString() + ":" + endTime.Minute.ToString("00.##") + " " + z.name + " - " + z.tutor + ", sala " + z.location + " rodzaj: " + z.type;
        }
        private void checkFor(string[,] tab, int i, Class z)
        {
            if (z.dateTime.Hour < 9)
            {
                tab[i, 0] = wyssijStringa(z);
                return;
            }
            if (z.dateTime.Hour < 11)
            {
                tab[i, 1] = wyssijStringa(z);
                return;
            }
            if (z.dateTime.Hour < 13)
            {
                tab[i, 2] = wyssijStringa(z);
                return;
            }
            if (z.dateTime.Hour < 15)
            {
                tab[i, 3] = wyssijStringa(z);
                return;
            }
            if (z.dateTime.Hour < 16)
            {
                tab[i, 4] = wyssijStringa(z);
                return;
            }
            if (z.dateTime.Hour < 18)
            {
                tab[i, 5] = wyssijStringa(z);
                return;
            }
            if (z.dateTime.Hour < 20)
            {
                tab[i, 6] = wyssijStringa(z);
                return;
            }
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        } //nie podświetlaj
        private void connectButton_Click(object sender, EventArgs e)
        {
            loadDB();
            loadStudents();
            loadButton.Enabled = false;
        }
        private void loadButton_Click(object sender, EventArgs e)
        {
            studenci = loadStudenci();
            loadStudents();
            zajecia = loadZajecia();
            loadButton.Enabled = false;
        }
        private void adminButton_Click(object sender, EventArgs e)
        {
            Admin admin = new Admin();
            admin.ShowDialog();
        }
    }
}
