﻿namespace Class_Schedule
{
    public class Student
    {
        public int id;
        public string name;
        public string secondname = "";
        public string surname;
        public string grupa;
        public Student(int id, string name, string secondname, string surname, string grupa)
        {
            this.id = id;
            this.name = name;
            this.secondname = secondname;
            this.surname = surname;
            this.grupa = grupa;
        }
        public Student(int id, string name, string surname, string grupa)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.grupa = grupa;
        }
        public Student()
        {

        }
        public string DisplayForm
        {
            get
            {
                return id.ToString() + ". \t" + name + "\t " + secondname + "\t " + surname + "\t\t " + grupa;
            }
        }
    }
}
