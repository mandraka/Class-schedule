﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Class_Schedule
{
    public partial class Admin : Form
    {
        List<Student> studenci = new List<Student>();
        List<Class> zajecia = new List<Class>();
        List<Student> studenciToAdd = new List<Student>();
        List<Student> studenciToRemove = new List<Student>();
        List<Class> zajeciaToAdd = new List<Class>();
        List<Class> zajeciaToRemove = new List<Class>();
        List<int> indicesToRemove = new List<int>(); //for db
        List<DateTime> datesToAdd = new List<DateTime>();
        string switcher; //s-studenci c-zajecia
        public Admin()
        {
            InitializeComponent();
            clearWindow();
            dateTimePicker1.Value = new DateTime(2000, 01, 01, 0, 0, 0);
        }
        private void clearWindow()
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            monthCalendar1.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            numericUpDown2.Visible = false;
            listBox1.Visible = false;
            addButton.Visible = false;
            deleteButton.Visible = false;
            executeButton.Visible = false;
            monthCalendar1.RemoveAllBoldedDates();
            datesToAdd.Clear();
            monthCalendar1.UpdateBoldedDates();
            dateTimePicker1.Visible = false;
            cwButton.Visible = false;
            wButton.Visible = false;
        }
        private void monthCalendar1_MouseDown(object sender, MouseEventArgs e)
        {
            MonthCalendar.HitTestInfo info = monthCalendar1.HitTest(e.Location);
            if (info.HitArea == MonthCalendar.HitArea.Date)
            {
                if (monthCalendar1.BoldedDates.Contains(info.Time))
                {
                    monthCalendar1.RemoveBoldedDate(info.Time);
                    datesToAdd.Remove(info.Time);
                }
                else
                {
                    monthCalendar1.AddBoldedDate(info.Time);
                    datesToAdd.Add(info.Time);
                }
                monthCalendar1.UpdateBoldedDates();
            }
        }
        private void studentsButton_Click(object sender, EventArgs e)
        {
            addButton.Enabled = true;
            deleteButton.Enabled = true;
            switcher = "s";
            clearWindow();
            listBox1.DataSource = null;
            studenci.Clear();
            label1.Visible = true;
            label1.Text = "Imię";
            textBox1.Visible = true;
            textBox1.Text = "";
            label2.Visible = true;
            label2.Text = "Drugie imię";
            textBox2.Visible = true;
            textBox2.Text = "";
            label3.Visible = true;
            label3.Text = "Nazwisko";
            textBox3.Visible = true;
            textBox3.Text = "";
            label4.Visible = true;
            label4.Text = "Grupa";
            textBox4.Visible = true;
            textBox4.Text = "";
            listBox1.Visible = true;
            addButton.Visible = true;
            deleteButton.Visible = true;
            executeButton.Visible = true;

            string connString = "Server=localhost;Uid=root;Pwd=1234;Database=pwsz;";
            MySqlConnection connection = new MySqlConnection(connString);
            connection.Open();
            string query = "SELECT idstudents, name, secondname, surname, grupa FROM students";
            var cmd = new MySqlCommand(query, connection);
            var reader = cmd.ExecuteReader();
            string temp = "";
            while (reader.Read())
            {
                try
                {
                    temp = reader.GetString(2);
                }
                catch { }
                Student x = new Student(reader.GetInt32(0), reader.GetString(1), temp, reader.GetString(3), reader.GetString(4));
                studenci.Add(x);
            }
            connection.Close();

            listBox1.DataSource = studenci;
            listBox1.DisplayMember = "DisplayForm";
        }
        private void classesButton_Click(object sender, EventArgs e)
        {
            addButton.Enabled = true;
            deleteButton.Enabled = true;
            switcher = "c";
            clearWindow();
            listBox1.DataSource = null;
            zajecia.Clear();
            label1.Visible = true;
            label1.Text = "Nazwa przedmiotu";
            textBox1.Visible = true;
            textBox1.Text = "";
            label2.Visible = true;
            label2.Text = "Wykładowca";
            textBox2.Visible = true;
            textBox2.Text = "";
            label3.Visible = true;
            label3.Text = "Grupa";
            if (cwButton.Checked == true)
            {
                textBox3.Visible = true;
                textBox3.Text = "";
            }
            else
            {
                textBox3.Visible = false;
                textBox3.Text = "";
            }
            label4.Visible = true;
            label4.Text = "Rodzaj";
            cwButton.Visible = true;
            wButton.Visible = true;
            textBox5.Visible = true;
            label5.Text = "Sala";
            label5.Visible = true;
            numericUpDown2.Visible = true;
            label6.Text = "Ilość godzin";
            label6.Visible = true;
            label7.Visible = true;
            monthCalendar1.Visible = true;
            listBox1.Visible = true;
            addButton.Visible = true;
            deleteButton.Visible = true;
            executeButton.Visible = true;
            dateTimePicker1.Visible = true;

            string connString = "Server=localhost;Uid=root;Pwd=1234;Database=pwsz;";
            MySqlConnection connection = new MySqlConnection(connString);
            connection.Open();
            string query = "SELECT idclasses, name, location, tutor, datetime, grupa, type, length FROM classes";
            var cmd = new MySqlCommand(query, connection);
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Class x = new Class(reader.GetInt16(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetDateTime(4), reader.GetString(5), reader.GetString(6), reader.GetInt16(7));
                zajecia.Add(x);
            }
            connection.Close();

            listBox1.DataSource = zajecia;
            listBox1.DisplayMember = "DisplayForm";
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            addAction();
        }
        private void addAction()
        {
            deleteButton.Enabled = false;
            //sprawdź czy pola zostały wypełnione
            if (switcher == "s" && (textBox1.Text != "" && textBox3.Text != "" && textBox4.Text != "") ||
                switcher == "c" && (textBox1.Text != "" && textBox2.Text != "" && (textBox3.Text != "" || wButton.Checked == true)))
            {
                listBox1.DataSource = null;
                DateTime merged = new DateTime();
                if (switcher == "s")
                {
                    int temp = 1;
                    if (studenci.Count > 0)
                        temp = studenci[studenci.Count - 1].id + 1;
                    Student s = new Student(temp, textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text);
                    studenci.Add(s);
                    studenciToAdd.Add(s);
                    listBox1.DataSource = studenci;
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                }
                else
                {
                    string rodzaj;
                    if (cwButton.Checked == true)
                        rodzaj = "ćw.";
                    else rodzaj = "w.";
                    foreach (DateTime dt in datesToAdd)
                    {
                        int temp = 1;
                        if (zajecia.Count > 0)
                            temp = zajecia[zajecia.Count - 1].id + 1;
                        merged = dt.Date + dateTimePicker1.Value.TimeOfDay;
                        Class c = new Class(temp, textBox1.Text, textBox5.Text, textBox2.Text, merged, textBox3.Text, rodzaj, (int)numericUpDown2.Value);
                        zajecia.Add(c);
                        zajeciaToAdd.Add(c);
                    }
                    listBox1.DataSource = zajecia;
                    monthCalendar1.RemoveAllBoldedDates();
                    monthCalendar1.UpdateBoldedDates();
                    datesToAdd.Clear();
                }
                listBox1.DisplayMember = "DisplayForm";
            }
            else
                MessageBox.Show("Proszę wypełnić brakujące pola!");
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            addButton.Enabled = false;
            if (switcher == "s")
            {
                foreach (int a in listBox1.SelectedIndices)
                {
                    indicesToRemove.Add(studenci[a].id);
                    studenciToRemove.Add(studenci[a]);
                }
                foreach (Student s in studenciToRemove)
                    studenci.Remove(s);
                listBox1.DataSource = null;
                listBox1.DataSource = studenci;
            }
            else
            {
                foreach (int a in listBox1.SelectedIndices)
                {
                    indicesToRemove.Add(zajecia[a].id);
                    zajeciaToRemove.Add(zajecia[a]);
                }
                foreach (Class c in zajeciaToRemove)
                    zajecia.Remove(c);
                listBox1.DataSource = null;
                listBox1.DataSource = zajecia;
            }
            listBox1.DisplayMember = "DisplayForm";
        }
        private void executeButton_Click(object sender, EventArgs e)
        {
            string connString = "Server=localhost;Uid=root;Pwd=1234;Database=pwsz;";
            MySqlConnection connection = new MySqlConnection(connString);
            connection.Open();
            if (switcher == "s")
            {
                foreach (Student s in studenciToAdd)
                {
                    string add = string.Format("INSERT INTO `pwsz`.`students` (`name`, `secondname`, `surname`, `grupa`) VALUES('{0}', '{1}', '{2}', '{3}');", s.name, s.secondname, s.surname, s.grupa);
                    var cmda = new MySqlCommand(add, connection);
                    cmda.ExecuteNonQuery();
                }
                foreach (int i in indicesToRemove)
                {
                    string rem = string.Format("DELETE FROM `pwsz`.`students` WHERE `idstudents`= '{0}'", i);
                    var cmdr = new MySqlCommand(rem, connection);
                    cmdr.ExecuteNonQuery();
                }
            }
            else
            {
                foreach (Class c in zajeciaToAdd)
                {
                    string temp = c.dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                    string add = string.Format("INSERT INTO `pwsz`.`classes` (`name`, `location`, `tutor`, `datetime`,`grupa`,`type`, `length`) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}');", c.name, c.location, c.tutor, temp, c.grupa, c.type, c.length);
                    var cmda = new MySqlCommand(add, connection);
                    cmda.ExecuteNonQuery();
                }
                foreach (int i in indicesToRemove)
                {
                    string rem = string.Format("DELETE FROM `pwsz`.`classes` WHERE `idclasses`= '{0}'", i);
                    var cmdr = new MySqlCommand(rem, connection);
                    cmdr.ExecuteNonQuery();
                }

            }
            connection.Close();
            clearWindow();
            studenciToAdd.Clear();
            zajeciaToAdd.Clear();
            indicesToRemove.Clear();
        }
        private void wButton_CheckedChanged(object sender, EventArgs e)
        {
            label3.Visible = false;
            textBox3.Visible = false;
            textBox3.Text = "";
        }
        private void cwButton_CheckedChanged(object sender, EventArgs e)
        {
            label3.Visible = true;
            textBox3.Visible = true;

        }
        #region Płynne textboxy
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Down))
                textBox2.Focus();
            if (e.KeyCode.Equals(Keys.Enter))
            {
                addAction();
                textBox1.Focus();
            }
        }
        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
                textBox1.Focus();
            if (e.KeyCode.Equals(Keys.Down) && switcher == "c" && wButton.Checked == true)
                textBox5.Focus();
            else if (e.KeyCode.Equals(Keys.Down))
                textBox3.Focus();
            if (e.KeyCode.Equals(Keys.Enter))
            {
                addAction();
                textBox1.Focus();
            }
        }
        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
                textBox2.Focus();
            if (e.KeyCode.Equals(Keys.Down) && switcher == "s")
                textBox4.Focus();
            else if (e.KeyCode.Equals(Keys.Down) && switcher == "c")
                textBox5.Focus();
            if (e.KeyCode.Equals(Keys.Enter))
            {
                addAction();
                textBox1.Focus();
            }
        }
        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up))
                textBox3.Focus();
            if (e.KeyCode.Equals(Keys.Enter))
            {
                addAction();
                textBox1.Focus();
            }
        }
        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up) && switcher == "c" && wButton.Checked == true)
                textBox2.Focus();
            else if (e.KeyCode.Equals(Keys.Up))
                textBox3.Focus();
            if (e.KeyCode.Equals(Keys.Enter))
            {
                addAction();
                textBox1.Focus();
            }
        }
        #endregion
    }
}
