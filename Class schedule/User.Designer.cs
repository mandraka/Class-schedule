﻿namespace Class_Schedule
{
    partial class User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            //stop clock
            t.Abort();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.listBoxA1 = new System.Windows.Forms.ListBox();
            this.showButton = new System.Windows.Forms.Button();
            this.listBoxA2 = new System.Windows.Forms.ListBox();
            this.listBoxB1 = new System.Windows.Forms.ListBox();
            this.radioA1 = new System.Windows.Forms.RadioButton();
            this.radioA2 = new System.Windows.Forms.RadioButton();
            this.radioB1 = new System.Windows.Forms.RadioButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Godzina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Poniedziałek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Wtorek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Środa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Czwartek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Piątek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sobota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Niedziela = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nextWeekButton = new System.Windows.Forms.Button();
            this.previousWeekButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.adminButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxA1
            // 
            this.listBoxA1.FormattingEnabled = true;
            this.listBoxA1.ItemHeight = 16;
            this.listBoxA1.Location = new System.Drawing.Point(174, 11);
            this.listBoxA1.Name = "listBoxA1";
            this.listBoxA1.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxA1.Size = new System.Drawing.Size(240, 132);
            this.listBoxA1.TabIndex = 0;
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(353, 196);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(336, 27);
            this.showButton.TabIndex = 1;
            this.showButton.Text = "Pokaż plan na obecny tydzień";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // listBoxA2
            // 
            this.listBoxA2.FormattingEnabled = true;
            this.listBoxA2.ItemHeight = 16;
            this.listBoxA2.Location = new System.Drawing.Point(415, 11);
            this.listBoxA2.Name = "listBoxA2";
            this.listBoxA2.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxA2.Size = new System.Drawing.Size(240, 132);
            this.listBoxA2.TabIndex = 3;
            // 
            // listBoxB1
            // 
            this.listBoxB1.FormattingEnabled = true;
            this.listBoxB1.ItemHeight = 16;
            this.listBoxB1.Location = new System.Drawing.Point(657, 11);
            this.listBoxB1.Name = "listBoxB1";
            this.listBoxB1.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxB1.Size = new System.Drawing.Size(240, 132);
            this.listBoxB1.TabIndex = 4;
            // 
            // radioA1
            // 
            this.radioA1.AutoSize = true;
            this.radioA1.Checked = true;
            this.radioA1.Location = new System.Drawing.Point(253, 164);
            this.radioA1.Name = "radioA1";
            this.radioA1.Size = new System.Drawing.Size(90, 21);
            this.radioA1.TabIndex = 7;
            this.radioA1.TabStop = true;
            this.radioA1.Text = "Grupa A1";
            this.radioA1.UseVisualStyleBackColor = true;
            // 
            // radioA2
            // 
            this.radioA2.AutoSize = true;
            this.radioA2.Location = new System.Drawing.Point(492, 164);
            this.radioA2.Name = "radioA2";
            this.radioA2.Size = new System.Drawing.Size(90, 21);
            this.radioA2.TabIndex = 8;
            this.radioA2.Text = "Grupa A2";
            this.radioA2.UseVisualStyleBackColor = true;
            // 
            // radioB1
            // 
            this.radioB1.AutoSize = true;
            this.radioB1.Location = new System.Drawing.Point(735, 164);
            this.radioB1.Name = "radioB1";
            this.radioB1.Size = new System.Drawing.Size(90, 21);
            this.radioB1.TabIndex = 9;
            this.radioB1.Text = "Grupa B1";
            this.radioB1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Godzina,
            this.Poniedziałek,
            this.Wtorek,
            this.Środa,
            this.Czwartek,
            this.Piątek,
            this.Sobota,
            this.Niedziela});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.Enabled = false;
            this.dataGridView1.Location = new System.Drawing.Point(11, 229);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 25;
            this.dataGridView1.RowTemplate.Height = 45;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1033, 340);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // Godzina
            // 
            this.Godzina.HeaderText = "Godzina";
            this.Godzina.Name = "Godzina";
            this.Godzina.ReadOnly = true;
            this.Godzina.Width = 90;
            // 
            // Poniedziałek
            // 
            this.Poniedziałek.HeaderText = "Poniedziałek";
            this.Poniedziałek.Name = "Poniedziałek";
            this.Poniedziałek.ReadOnly = true;
            this.Poniedziałek.Width = 117;
            // 
            // Wtorek
            // 
            this.Wtorek.HeaderText = "Wtorek";
            this.Wtorek.Name = "Wtorek";
            this.Wtorek.ReadOnly = true;
            this.Wtorek.Width = 82;
            // 
            // Środa
            // 
            this.Środa.HeaderText = "Środa";
            this.Środa.Name = "Środa";
            this.Środa.ReadOnly = true;
            this.Środa.Width = 75;
            // 
            // Czwartek
            // 
            this.Czwartek.HeaderText = "Czwartek";
            this.Czwartek.Name = "Czwartek";
            this.Czwartek.ReadOnly = true;
            this.Czwartek.Width = 94;
            // 
            // Piątek
            // 
            this.Piątek.HeaderText = "Piątek";
            this.Piątek.Name = "Piątek";
            this.Piątek.ReadOnly = true;
            this.Piątek.Width = 76;
            // 
            // Sobota
            // 
            this.Sobota.HeaderText = "Sobota";
            this.Sobota.Name = "Sobota";
            this.Sobota.ReadOnly = true;
            this.Sobota.Width = 82;
            // 
            // Niedziela
            // 
            this.Niedziela.HeaderText = "Niedziela";
            this.Niedziela.Name = "Niedziela";
            this.Niedziela.ReadOnly = true;
            this.Niedziela.Width = 95;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(864, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 29);
            this.label2.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(864, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 29);
            this.label3.TabIndex = 15;
            // 
            // nextWeekButton
            // 
            this.nextWeekButton.Location = new System.Drawing.Point(695, 196);
            this.nextWeekButton.Name = "nextWeekButton";
            this.nextWeekButton.Size = new System.Drawing.Size(169, 27);
            this.nextWeekButton.TabIndex = 16;
            this.nextWeekButton.Text = "Pokaż plan na następny tydzień";
            this.nextWeekButton.UseVisualStyleBackColor = true;
            this.nextWeekButton.Click += new System.EventHandler(this.nextWeekButton_Click);
            // 
            // previousWeekButton
            // 
            this.previousWeekButton.Location = new System.Drawing.Point(171, 196);
            this.previousWeekButton.Name = "previousWeekButton";
            this.previousWeekButton.Size = new System.Drawing.Size(169, 27);
            this.previousWeekButton.TabIndex = 17;
            this.previousWeekButton.Text = "Pokaż plan na poprzedni tydzień";
            this.previousWeekButton.UseVisualStyleBackColor = true;
            this.previousWeekButton.Click += new System.EventHandler(this.previousWeekButton_Click);
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(3, 13);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(169, 23);
            this.connectButton.TabIndex = 18;
            this.connectButton.Text = "Pobierz bazę danych z serwera";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(3, 43);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(169, 23);
            this.loadButton.TabIndex = 19;
            this.loadButton.Text = "Wczytaj bazę danych z pliku";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // adminButton
            // 
            this.adminButton.Location = new System.Drawing.Point(915, 13);
            this.adminButton.Name = "adminButton";
            this.adminButton.Size = new System.Drawing.Size(127, 37);
            this.adminButton.TabIndex = 20;
            this.adminButton.Text = "Pokaż panel administratora";
            this.adminButton.UseVisualStyleBackColor = true;
            this.adminButton.Click += new System.EventHandler(this.adminButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(24, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "label1";
            // 
            // User
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1054, 570);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.adminButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.previousWeekButton);
            this.Controls.Add(this.nextWeekButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.radioB1);
            this.Controls.Add(this.radioA1);
            this.Controls.Add(this.radioA2);
            this.Controls.Add(this.listBoxB1);
            this.Controls.Add(this.listBoxA2);
            this.Controls.Add(this.showButton);
            this.Controls.Add(this.listBoxA1);
            this.MaximizeBox = false;
            this.Name = "User";
            this.Text = "Plan zajęć";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxA1;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.ListBox listBoxA2;
        private System.Windows.Forms.ListBox listBoxB1;
        private System.Windows.Forms.RadioButton radioA1;
        private System.Windows.Forms.RadioButton radioA2;
        private System.Windows.Forms.RadioButton radioB1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Godzina;
        private System.Windows.Forms.DataGridViewTextBoxColumn Poniedziałek;
        private System.Windows.Forms.DataGridViewTextBoxColumn Wtorek;
        private System.Windows.Forms.DataGridViewTextBoxColumn Środa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Czwartek;
        private System.Windows.Forms.DataGridViewTextBoxColumn Piątek;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sobota;
        private System.Windows.Forms.DataGridViewTextBoxColumn Niedziela;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button nextWeekButton;
        private System.Windows.Forms.Button previousWeekButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button adminButton;
        private System.Windows.Forms.Label label1;
    }
}

