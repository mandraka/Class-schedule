﻿using System;

namespace Class_Schedule
{
    public class Class
    {
        public int id;
        public string name;
        public string tutor = "";
        public string type;
        public string grupa;
        public string location;
        public int length;
        public DateTime dateTime;

        public Class(int id, string name, string location, string tutor, DateTime dateTime, string grupa, string type, int length)
        {
            this.id = id;
            this.name = name;
            this.tutor = tutor;
            this.type = type;
            this.grupa = grupa;
            this.location = location;
            this.dateTime = dateTime;
            this.length = length;
        }
        public Class()
        {
        }
        public string DisplayForm
        {
            get
            {
                return id.ToString() + ".\t" + name + "\t" + location + "\t" + tutor + "\t" + dateTime + "\t" + grupa + "\t" + type + "\t" + length;
            }
        }
    }
}
